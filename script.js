// console.log("Thursday na!")

let getCubeofFour = 4 ** 3;
console.log(`The cube of 4 is ${getCubeofFour}`);


let address = [24, "Saint Martin Street", "San Andress", "Cainta", "Rizal", "Philippines", 1900];

 let [houseNumber, street, barangay, municipality, province, country, zipCode] = address;


console.log(`I live at ${houseNumber} ${street}, ${barangay}, ${municipality}, ${province}, ${country} ${zipCode}`);

let animal = {
	name: "Lolong",
	type: "salt water crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in",
};

let {name, type, weight, length} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight} with measurement of ${length}.`)


/*Create an array of numbers.
Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
Create/instantiate a new object from the class Dog and console log the object.
Create a git repository named S24.
Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
Add the link in Boodle.
*/


let numberArray = [1, 2, 3, 4, 5]

numberArray.forEach(function(number) {
     
     console.log(number);

     });

// Implicit Return
let reduceNumber = numberArray.reduce((x,y) => x  + y);

console.log(reduceNumber);


class Dog {
	constructor(name, age, breed, size, grooming) {
		this.name = name;
		this.age = age;
		this.breed = breed;
		this.size = size;
		this.grooming = grooming;
	}
}

let dog1 = new Dog ("Whiskey", 4, "Bichon Frise", "Small", "Considerable Grooming");

let dog2 = new Dog ("Champaigne", 2, "Groenendael", "Medium", "Moderate Grooming");

let dog3 = new Dog ("Wine", 5, "Bloodhound", "Large", "Easy Grooming");

console.log(dog1);
console.log(dog2);
console.log(dog3);